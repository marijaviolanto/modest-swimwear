<?php 
if (is_single()){
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: ".get_bloginfo('url'));
	exit();
}
?>
<?php get_header(); // add header ?>
<div class="wrap-fullwidth">

    <div class="single-content">
        <div class="entry-top">
        <h1 class="article-title entry-title">Sorry, the page you are looking for has gone shopping, but there are plenty alternatives around. Keep looking!</h1> 
        </div><div class="clear">
    </div>
       

<?php echo do_shortcode('[mctagmap columns="4" hide="yes" num_show="30" more="more »" toggle="« less" show_empty="no" name_divider="|" tag_count="yes" show_pages="no" exclude="2248" width="170" equal="yes" manual="" basic ="no" basic_heading="no" show_categories="yes"]'); ?>
    </div>
    
    <!-- Begin Sidebar (right) -->
    <?php  get_sidebar(); // add sidebar ?>
    <!-- end #sidebar  (right) -->    
        
    <div class="clear"></div>
    
    <!--banner-->
    <a target='new' href="http://click.linksynergy.com/fs-bin/click?id=9LaR6VyOH9Q&offerid=391503.167&subid=0&type=4" class="red-banner"><IMG border="0"   alt="Lord & Taylor" src="http://ad.linksynergy.com/fs-bin/show?id=9LaR6VyOH9Q&bids=391503.167&subid=0&type=4&gridnum=0"></a>
    <!--end banner-->

</div>


<?php get_footer(); // add footer  ?>