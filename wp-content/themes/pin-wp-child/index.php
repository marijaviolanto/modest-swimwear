<?php get_header(); // add header  ?>
<?php
// Options from admin panel
global $smof_data;

if (empty($smof_data['banner_p1'])) {
    $smof_data['banner_p1'] = '';
}
if (empty($smof_data['banner_p2'])) {
    $smof_data['banner_p2'] = '';
}
if (empty($smof_data['banner_p3'])) {
    $smof_data['banner_p3'] = '';
}
if (empty($smof_data['banner_p3'])) {
    $smof_data['banner_p4'] = '';
}
if (empty($smof_data['banner_p3'])) {
    $smof_data['banner_p5'] = '';
}
$home_pag_select = (isset($smof_data['home_pag_select'])) ? $smof_data['home_pag_select'] : 'Infinite Scroll';
$display_ads = (isset($smof_data['display_ads'])) ? $smof_data['display_ads'] : 'Yes';
?>

<!-- Begin Home Full width -->
<div class="home-fullwidth">

    <!-- Begin Sidebar (left) -->
    <?php get_template_part('sidebar2'); ?>
    <!-- end #sidebar (left) -->

    <!-- Begin Main Wrap Content -->
    <div class="wrap-content">
        <div class="wrap-content-inner">
            <?php if (is_home()): ?>
                <div class="shortcut">
                    <div class="cf">

                        <div class="shortcut-child">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/home-cats-womens-w250.png"
                                    alt="women"/>
                                <h3>Styles</h3>
                            <a href="http://modestswimwear.com.au/bikinis/"><p class="shortcut-text">Bikinis</p></a>
                            <a href="http://modestswimwear.com.au/one-pieces/"><p class="shortcut-text">One pieces</p></a>
                            <a href="http://modestswimwear.com.au/bikini-tops/"><p class="shortcut-text">Bikini tops</p></a>
                        </div>

                        <div class="shortcut-child">
                           <img src="<?php bloginfo('stylesheet_directory'); ?>/images/home-cats--designers-w250.png"
                                    alt="designers"/>
                                <h3>Brands</h3>
                            <a href="http://modestswimwear.com.au/seafolly/"><p class="shortcut-text">Seafolly</p></a>
                            <a href="http://modestswimwear.com.au/roxy/"><p class="shortcut-text">Roxy</p></a>
                            <a href="http://modestswimwear.com.au/mara-hoffman/"><p class="shortcut-text">Mara Hoffman</p></a>
                        </div>

                        <div class="shortcut-child">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/home-cats-designerbags-w250.png"
                                    alt="designers"/>
                                <h3>And more</h3>
                            <a href="http://modestswimwear.com.au/cover-ups/"><p class="shortcut-text">Cover ups</p></a>
                            <a href="http://modestswimwear.com.au/body-slimming/"><p class="shortcut-text">Body Slimming</p>
                            </a>
                            <a href="http://modestswimwear.com.au/category/plus-sizes/"><p class="shortcut-text">Plus Sizes</p>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (is_category()) { ?>
                <div class="archive-header"><h1><?php esc_html_e('Shop', 'anthemes'); ?>
                        <strong><?php single_cat_title(''); ?></strong></h1></div>
            <?php } elseif (is_tag()) { ?>
                <div class="archive-header"><h1><?php esc_html_e('All posts tagged in:', 'anthemes'); ?>
                        <strong><?php single_tag_title(''); ?></strong></h1></div>
            <?php } elseif (is_search()) { ?>
                <div class="archive-header">
                    <h1><?php printf(esc_html__('Search Results for: %s', 'anthemes'), '<strong>' . get_search_query() . '</strong>'); ?></h1>
                </div>
            <?php } elseif (is_author()) { ?>
                <div class="archive-header"><h1><?php esc_html_e('All posts by:', 'anthemes'); ?>
                        <strong><?php the_author(); ?></strong></h1></div>
            <?php } elseif (is_404()) { ?>
                <div class="archive-header"><h1><?php esc_html_e('Error 404 - Not Found', 'anthemes'); ?>
                        <br/> <?php esc_html_e('Sorry, but you are looking for something that isn\'t here.', 'anthemes'); ?>
                    </h1></div>
            <?php } elseif (is_home()) { ?>
                <h1 class="designer-clothes"> Shop designer clothes and footwear by top brands</h1>
            <?php } ?>


            <div class="archive-header">
                <?php include get_stylesheet_directory() . '/common/filter-frontend.php'; ?>
            </div>

            <ul id="infinite-articles" class="masonry_list js-masonry" data-masonry-options='{ "columnWidth": 0 }'>
                <?php $num = 0;
                if (have_posts()) : while (have_posts()) : the_post();
                    $num++; ?>

                    <?php if ($display_ads == 'Yes') { ?>
                        <?php if (!empty($smof_data['banner_300_1'])) { ?>
                            <?php if ($num == $smof_data['banner_p1']) {
                                echo '<li class="homeadv">' . stripslashes($smof_data['banner_300_1']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_2'])) { ?>
                            <?php if ($num == $smof_data['banner_p2']) {
                                echo '<li class="homeadv">' . stripslashes($smof_data['banner_300_2']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_3'])) { ?>
                            <?php if ($num == $smof_data['banner_p3']) {
                                echo '<li class="homeadv">' . stripslashes($smof_data['banner_300_3']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_4'])) { ?>
                            <?php if ($num == $smof_data['banner_p4']) {
                                echo '<li class="homeadv">' . stripslashes($smof_data['banner_300_4']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_5'])) { ?>
                            <?php if ($num == $smof_data['banner_p5']) {
                                echo '<li class="homeadv">' . stripslashes($smof_data['banner_300_5']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if (!empty($smof_data['banner_300_1'])) { ?>
                            <?php if ($num == $smof_data['banner_p1']) {
                                echo '<li class="ex34 homeadv">' . stripslashes($smof_data['banner_300_1']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_2'])) { ?>
                            <?php if ($num == $smof_data['banner_p2']) {
                                echo '<li class="ex34 homeadv">' . stripslashes($smof_data['banner_300_2']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_3'])) { ?>
                            <?php if ($num == $smof_data['banner_p3']) {
                                echo '<li class="ex34 homeadv">' . stripslashes($smof_data['banner_300_3']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_4'])) { ?>
                            <?php if ($num == $smof_data['banner_p4']) {
                                echo '<li class="ex34 homeadv">' . stripslashes($smof_data['banner_300_4']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_5'])) { ?>
                            <?php if ($num == $smof_data['banner_p5']) {
                                echo '<li class="ex34 homeadv">' . stripslashes($smof_data['banner_300_5']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                    <?php } ?>

                    <li <?php post_class('ex34') ?> id="post-<?php the_ID(); ?>">
                        <a href="<?php the_permalink(); ?>" rel="nofollow" target="_blank">
                            <?php if (rwmb_meta('_outOfStock')): ?>
                                <span class="out-of-stock">Out of stock</span>
                            <?php endif ?>
                            <img class="attachment-thumbnail-blog-masonry wp-post-image "
                                 onerror="this.onerror=null;this.src='<?php echo get_stylesheet_directory_uri() . '/images/image-coming-soon.png' ?>';"src="<?php echo rwmb_meta('_image') ?>"/></a>


                        <div class="clear"></div>

                        <div class="small-content">
                            <div class="an-widget-title">
                                <h2 class="article-title entry-title underline-detail">
                                    <a class="product-popup" title="<?php the_title() ?>"
                                       href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                <?php if (function_exists('taqyeem_get_score')) { ?>
                                    <?php taqyeem_get_score(); ?>
                                <?php } ?>
                            </div>
                            <p class="price">
                                <?php if (rwmb_meta('_salePrice')) { ?>
                                    <span class="old-price">
                            <?php st_the_price() ?>
                        </span><span class="now">now</span>
                                    <span class="sale-price">
                            <?php st_the_price(true) ?>
                        </span>
                                <?php } else { ?>
                                    <span class="regular-price">
                            <?php st_the_price() ?>
                        </span>
                                <?php } ?>
                            </p>
                            <p><?php echo anthemes_excerpt(strip_tags(strip_shortcodes(get_the_excerpt())), 80); ?></p>
                        </div><!-- end .small-content -->
                        <a href="<?php echo rwmb_meta('_buyUrl') ?>" rel="nofollow" target="_blank" class="btn-st">Make
                            Me Yours<i class="fa fa-angle-right"></i></a>
                    </li>
                <?php endwhile; endif; ?>
            </ul><!-- end .masonry_list -->

            <!-- Pagination -->
            <?php if ($home_pag_select == 'Infinite Scroll') { ?>
                <div id="nav-below" class="pagination" style="display: none;">
                    <div
                        class="nav-previous"><?php previous_posts_link('&lsaquo; ' . esc_html__('Newer Entries', 'anthemes') . ''); ?></div>
                    <div
                        class="nav-next"><?php next_posts_link('' . esc_html__('Older Entries', 'anthemes') . ' &rsaquo;'); ?></div>
                </div>
            <?php } else { // Infinite Scroll ?>
                <div class="clear"></div>
                <?php if (function_exists('wp_pagenavi')) { ?>
                    <?php wp_pagenavi(); ?>
                <?php } else { ?>
                    <div class="defaultpag">
                        <div
                            class="sright"><?php next_posts_link('' . esc_html__('Older Entries', 'anthemes') . ' &rsaquo;'); ?></div>
                        <div
                            class="sleft"><?php previous_posts_link('&lsaquo; ' . esc_html__('Newer Entries', 'anthemes') . ''); ?></div>
                    </div>
                <?php } ?>
            <?php } // Default Pagination ?>
            <!-- pagination -->
        </div>
    </div><!-- end .wrap-content -->


    <div class="clear"></div>
</div><!-- end .home-fullwidth -->

<script>
    (function ($) {
        $(function () {
            $dialog = $('.single-preview-dialog');
            $(document).on('click', '.product-popup', function (e) {
                return;
                e.preventDefault();

                var $this = $(this);
                $dialog.load($this.prop('href') + ' .media-product', function () {
                    $dialog.dialog({
                        title: $this.prop('title'),
                        dialogClass: 'products-details-dialog',
                        draggable: false,
                        modal: true,
                        width: $(window).width() * 0.8
                    });
                });
            });
        });
    })(jQuery);
</script>
<div class="single-preview-dialog"></div>
<?php get_footer(); // add footer  ?>
